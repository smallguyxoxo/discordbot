const Discord = require("discord.js");
const config = require("./config.json");
const axios = require("axios");

const client = new Discord.Client();
client.login(config.BOT_TOKEN);

client.on("message", async function (message) {
    if (message.author.bot) return;
    if (message.content.startsWith("!")) {
        let command = message.content;
        let arr = command.split(" ");
        console.log(arr);
        if (arr[0] == "!all" && arr[1] != null && arr.length == 2) {
            let x = await fetchAll(arr[1]);
            message.reply(x);
        } else {
            message.reply(
                "Err: Command not found\nElectricity I get, People I don't"
            );
        }
    }
});

async function fetchAll(name) {
    let data;
    await axios.get("https://api.covid19api.com/summary").then((res) => {
        data = res.data.Countries;
        //console.log(res.data.Countries);
    });
    //data.find(el => el.Country == name).then(ret => console.log(ret))
    for (let i = 0; i <= 190; i++) {
        if (data[i].Country === name || data[i].slug === name) {
            return data[i];
            //return data[i]
        }
    }
    return "Country Not Found";
}
